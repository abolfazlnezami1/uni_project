import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:uni_project/feature/home/data/models/home_model.dart';

abstract class HomeRemoteDataSource {
  Future<List<HomeEntityModel>> getData(int page);
}

class HomeRemoteDataSourceImpl extends HomeRemoteDataSource {
  @override
  Future<List<HomeEntityModel>> getData(int page) async {
    try {
      var url = Uri.parse(
          'https://api.nomics.com/v1/currencies/ticker?key=a6de0f83c9dc7aae8237599ebebc152674dfdd11&convert=USD&per-page=50&page=$page');
      var response = await http.get(
        url,
      );
      if (response.statusCode == 200) {
        final decodeJson = json.decode(response.body);
        print(decodeJson);
        List<HomeEntityModel> homeEntityModelList = List<HomeEntityModel>.from(
          decodeJson.map(
            (x) => HomeEntityModel.fromJson(x),
          ),
        );
        return homeEntityModelList;
      } else {
        throw FormatException(
            (jsonDecode(response.body))["message"].toString());
      }
    } catch (e) {
      throw Exception();
    }
  }
}
