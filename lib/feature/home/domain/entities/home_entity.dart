import 'package:equatable/equatable.dart';

class HomeEntity extends Equatable {
  final String? id;
  final String? currency;
  final String? symbol;
  final String? name;
  final String? logoUrl;
  final String? price;
  final String? circulatingSupply;
  final String? maxSupply;
  final String? marketCap;
  final String? marketCapDominance;
  final String? numExchanges;
  final String? numPairs;
  final String? numPairsUnmapped;
  final String? rank;
  final String? rankDelta;
  final String? high;
  final The1D? the1D;
  final The1D? the7D;
  final The1D? the30D;
  final The1D? the365D;
  final The1D? ytd;
  final String? platformCurrency;
  final DateTime? firstPricedAt;
  final DateTime? priceDate;
  final DateTime? priceTimestamp;
  final DateTime? highTimestamp;
  final DateTime? firstCandle;
  final DateTime? firstTrade;
  final DateTime? firstOrderBook;

  const HomeEntity({
    this.id,
    this.currency,
    this.symbol,
    this.name,
    this.logoUrl,
    this.price,
    this.circulatingSupply,
    this.maxSupply,
    this.marketCap,
    this.marketCapDominance,
    this.numExchanges,
    this.numPairs,
    this.numPairsUnmapped,
    this.rank,
    this.rankDelta,
    this.high,
    this.the1D,
    this.the7D,
    this.the30D,
    this.the365D,
    this.ytd,
    this.platformCurrency,
    this.firstPricedAt,
    this.priceDate,
    this.priceTimestamp,
    this.highTimestamp,
    this.firstCandle,
    this.firstTrade,
    this.firstOrderBook,
  });

  @override
  List<Object?> get props => [
        id,
        currency,
        symbol,
        name,
        logoUrl,
        price,
        circulatingSupply,
        maxSupply,
        marketCap,
        marketCapDominance,
        numExchanges,
        numPairs,
        numPairsUnmapped,
        rank,
        rankDelta,
        high,
        the1D,
        the7D,
        the30D,
        the365D,
        ytd,
        platformCurrency,
        firstPricedAt,
        priceDate,
        priceTimestamp,
        highTimestamp,
        firstCandle,
        firstTrade,
        firstOrderBook,
      ];
}

class The1D extends Equatable {
  final String? volume;
  final String? priceChange;
  final String? priceChangePct;
  final String? volumeChange;
  final String? volumeChangePct;
  final String? marketCapChange;
  final String? marketCapChangePct;

  const The1D({
    this.volume,
    this.priceChange,
    this.priceChangePct,
    this.volumeChange,
    this.volumeChangePct,
    this.marketCapChange,
    this.marketCapChangePct,
  });

  @override
  List<Object?> get props => [
        volume,
        priceChange,
        priceChangePct,
        volumeChange,
        volumeChangePct,
        marketCapChange,
        marketCapChangePct,
      ];
}
